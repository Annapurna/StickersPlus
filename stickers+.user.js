// ==UserScript==
// @name         Stickers+
// @namespace    http://tampermonkey.net/
// @version      2.0
// @description  améliore la gestion des stickers
// @author       Annapurna
// @match        https://onche.org/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=onche.org
// @grant        none
// ==/UserScript==

(function () {
    'use strict';


    resetScriptOnFixUpdate();
    // Retrieve the value of 'stickersNoScroll' from local storage
    const stickersNoScrollValue = localStorage.getItem('stickersNoScroll');
    const stickersSynchronize = localStorage.getItem('stickersynchronize');

    const stickersContainer = document.querySelector('.favoriteStickers');
    console.log("Stickers+ starts displaying stickers. Enjoy.");


    /*
        -------------------------------------------------------------------------------------------------------------------
        THIS PART INITIALISES ALL CURRENT FAVORITE STICKERS WHEN THE SCRIPT IS USED FOR THE FIRST TIME AND OTHER THINGS
        -------------------------------------------------------------------------------------------------------------------
    */

    function initialiseStickers() {
        if (window.location.href.startsWith("https://onche.org/forum/")) {
            const token = document.querySelector('#forum').getAttribute('data-token');
            localStorage.setItem('stickertoken', token);
        }
        else if (window.location.href.startsWith("https://onche.org/topic/")) {
            const token = document.querySelector('#topic').getAttribute('data-token');
            localStorage.setItem('stickertoken', token);
        }

        // Check if "isInitialised" key exists in local storage
        if (!localStorage.getItem('isInitialised') || !localStorage.getItem('stickfolders')) {
            localStorage.setItem('isInitialised', 'true');

            // Check if 'stickersNoScroll' item exists in local storage
            const stickersNoScrollExists = localStorage.getItem('stickersNoScroll') !== null;

            if (!stickersNoScrollExists) {
                localStorage.setItem('stickersNoScroll', 'false');
            }


            const stickersSynchronizeExists = localStorage.getItem('stickersynchronize') !== null;

            if (!stickersSynchronizeExists) {
                localStorage.setItem('stickersynchronize', 'false');
            }

            // Get the 'stickers' item from local storage
            var stickersItem = localStorage.getItem('stickers');

            // Check if 'stickers' item exists in local storage
            if (stickersItem) {
                // Parse the JSON string to an object
                var stickersData = JSON.parse(stickersItem);

                // Create an array to store the extracted properties
                var stickfoldersData = [];
                var position = 1;
                // Iterate over each object in the 'data' array
                stickersData.data.forEach(function (sticker) {
                    // Extract the properties you want
                    var stickfolderData = {
                        name: sticker.name,
                        link: sticker.image,
                        collection: sticker.collection_id,
                        folder: 'racine',
                        position: position,
                        isCollection: false
                    };

                    // Push the extracted properties to the new array
                    stickfoldersData.push(stickfolderData);
                    position++;
                });

                // Store the new array in local storage under the key 'stickfolders'
                localStorage.setItem('stickfolders', JSON.stringify(stickfoldersData));
            }
        }
    }

    function addFromOtherDevices() {
        // Retrieve items from local storage
        let stickersObj = JSON.parse(localStorage.getItem('stickers'));
        let stickfoldersArr = JSON.parse(localStorage.getItem('stickfolders'));

        if (stickersObj && stickfoldersArr) {
            // Extract names from stickersObj and stickfoldersArr
            const stickerNames = stickersObj.data.map(sticker => sticker.name);
            const stickfolderNames = stickfoldersArr.map(stickfolder => stickfolder.name);

            // Find names in stickersObj that are not in stickfoldersArr
            const namesNotInStickfolders = stickerNames.filter(name => !stickfolderNames.includes(name));

            // Insert stickers data not in stickfolders
            namesNotInStickfolders.forEach(name => {
                // Find sticker data by name
                const stickerData = stickersObj.data.find(sticker => sticker.name === name);
                const position = getFolderLength('racine') + 1;

                // Make necessary changes
                const modifiedStickerData = {
                    name: stickerData.name,
                    link: stickerData.image, // Change 'image' to 'link'
                    collection: stickerData.collection_id,
                    folder: "racine", // Add 'folder' field with value "racine"
                    position: position,
                    isCollection: false
                };
                // Insert modified sticker data into stickfoldersArr
                stickfoldersArr.push(modifiedStickerData);
            });

            // Update 'stickfolders' in local storage
            localStorage.setItem('stickfolders', JSON.stringify(stickfoldersArr));
        }
    }

    function removeFromOtherDevices() {
        // Retrieve items from local storage
        let stickersObj = JSON.parse(localStorage.getItem('stickers'));
        let stickfoldersArr = JSON.parse(localStorage.getItem('stickfolders'));

        if (stickersObj && stickfoldersArr) {
            // Extract names from stickersObj and stickfoldersArr
            const stickerNames = stickersObj.data.map(sticker => sticker.name);
            const stickfolderNames = stickfoldersArr.map(stickfolder => stickfolder.name);

            // Find names in stickfoldersArr that are not in stickersObj
            const namesNotInStickers = stickfolderNames.filter(name => !stickerNames.includes(name));

            // Remove stickers data not in stickersObj from stickfoldersArr
            namesNotInStickers.forEach(name => {
                // Find index of sticker data in stickfoldersArr by name
                const index = stickfoldersArr.findIndex(stickfolder => stickfolder.name === name);

                // Remove sticker data from stickfoldersArr
                if (index !== -1 && !stickfoldersArr[index].isCollection) {
                    updateStickersPositionOnFolderChangeOrDelete(name);
                    stickfoldersArr.splice(index, 1);
                }
            });

            // Update 'stickfolders' in local storage
            localStorage.setItem('stickfolders', JSON.stringify(stickfoldersArr));
        }
    }


    function resetScriptButton() {
        const blocDiv = document.querySelector('#right .purple .centered');
        // Create the <a> element
        const aElement = document.createElement('a');
        aElement.id = 'author-only-button';
        aElement.className = 'button bordered don';
        aElement.target = '_blank';

        // Create the <div> element for the icon
        const divIcon = document.createElement('div');
        divIcon.className = 'mdi mdi-ghost';

        // Create the text node for the button label
        const textNode = document.createTextNode('[Stickers+] Réinitialisez le script');

        // Append the <div> icon and the text node to the <a> element
        aElement.appendChild(divIcon);
        aElement.appendChild(textNode);

        // Append the <a> element to the document body (or any other desired parent element)
        blocDiv.appendChild(aElement);
        aElement.addEventListener('click', () => {
            localStorage.removeItem('stickfolders');
        })
    }

    function resetScriptOnFixUpdate() {
        // Check if the 'notification' item exists in local storage
        const stickerupdate = localStorage.getItem('stickerupdate');

        // If 'notification' item doesn't exist or is null, create it and set it to 'false'
        if (stickerupdate === null) {
            localStorage.setItem('stickerupdate', 1);
            localStorage.removeItem('stickfolders');
            localStorage.removeItem('stickertoken');
            localStorage.removeItem('stickers');
            localStorage.removeItem('isInitialised');
        }
    }

    function synchronizeStickersFromCloud() {
        if (stickersSynchronize) {
            addFromOtherDevices();
            removeFromOtherDevices();
        }
    }

    synchronizeStickersFromCloud();
    resetScriptButton();
    initialiseStickers();


    if (window.location.href.startsWith("https://onche.org/forum/") || window.location.href.startsWith("https://onche.org/topic/")) {
        clickOnSticker();
        setFavoritestickers(stickersContainer);

        // Check if the value is 'true'
        if (stickersNoScrollValue === 'true') {
            // Perform your action here, for example:
            stickersContainer.style.maxHeight = 'none';
        }
    }


    /*
        -------------------------------------------------------------------------------------------------------------------
        THIS PART MANAGES WHEN A STICKER IS ADDED TO FAVORITES
        -------------------------------------------------------------------------------------------------------------------
    */

    //allow to manage when a sticker is added to favorites
    function clickOnSticker() {
        var stickerElements = document.querySelectorAll('.sticker');
        stickerElements.forEach(function (stickerElement) {
            stickerElement.addEventListener('click', function (event) {

                // Get data attributes
                var dataName = stickerElement.getAttribute('data-name');
                var dataCollection = stickerElement.getAttribute('data-collection');

                // Get image source as a child of the parent element
                var imgElement = stickerElement.querySelector('img');
                var imgSrc = imgElement.getAttribute('src');

                // Get the position and size of the 'contextMenu'
                var contextMenu = document.getElementById('context');

                // Define the function
                var clickHandler = function (event) {
                    var clickedItem = event.target;
                    if (clickedItem.classList.contains('item') && clickedItem.textContent.trim() === 'Ajouter aux favoris') {
                        addStickerLocalstorage(dataName, imgSrc, dataCollection, 'racine', false);
                        setTimeout(function () {
                            setFavoritestickers(stickersContainer);
                        }, 500);
                        displayCustomMenu(dataName, imgSrc, dataCollection, '', 1);
                    }
                    else if (clickedItem.classList.contains('item') && clickedItem.textContent.trim() === 'Voir la collection') {
                        setTimeout(function () {
                            handleCollection();
                        }, 500);
                    }
                };

                // Wait for a short time (e.g., 100 milliseconds) for the context menu to appear
                setTimeout(function () {
                    contextMenu.addEventListener('click', clickHandler);

                    var contextMenuClose = document.getElementById('context');

                    var observer = new MutationObserver(function (mutations) {
                        mutations.forEach(function (mutation) {
                            if (mutation.attributeName === 'style') {
                                if (contextMenuClose.style.display === 'none') {
                                    setTimeout(function () {
                                        contextMenuClose.removeEventListener('click', clickHandler);
                                        observer.disconnect();
                                    }, 100);
                                }
                            }
                        });
                    });

                    // Configure and start the observer
                    var observerConfig = { attributes: true };
                    observer.observe(contextMenuClose, observerConfig);
                }, 100);
            });
        });
    }

    //affiche la boîte modale permettant de choisir le dossier de destination du sticker ajouté en favori
    function displayCustomMenu(name, link, collection, folder, call) {

        // Retrieve folder names from localStorage
        var folderNames = JSON.parse(localStorage.getItem('folders')) || [];

        // Dynamically create modal elements
        const modal = document.createElement('div');
        modal.className = 'modal';
        document.body.appendChild(modal);

        const modalContent = document.createElement('div');
        modalContent.className = 'modal-content';
        modalContent.style.backgroundColor = '#1c2229';
        modal.appendChild(modalContent);

        const closeBtn = document.createElement('span');
        closeBtn.className = 'close';
        closeBtn.style.color = 'white';  // Set text color
        closeBtn.innerHTML = '&times;';
        modalContent.appendChild(closeBtn);

        var title = document.createElement('div');
        title.className = 'title';
        title.textContent = "Dossier où ranger le sticker";
        title.style.color = 'white';  // Set text color
        title.style.paddingBottom = "10px";
        modalContent.appendChild(title);

        var addFolderParent = document.createElement('div');
        addFolderParent.style.display = 'flex';
        addFolderParent.style.fontSize = '14px';

        var addFolder = document.createElement('input');
        addFolder.type = 'text';
        addFolder.placeholder = 'Créez un dossier';
        addFolder.style.width = '100%';
        addFolder.style.marginRight = '10px';




        // Create SVG element
        var svgElement = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svgElement.setAttribute("width", "24px");
        svgElement.setAttribute("height", "24px");
        svgElement.setAttribute("viewBox", "0 0 24 24");
        svgElement.style.cursor = 'pointer';
        svgElement.style.color = 'white';


        // Create path element
        var pathElement = document.createElementNS("http://www.w3.org/2000/svg", "path");
        pathElement.setAttribute("fill", "none");
        pathElement.setAttribute("stroke", "white");
        pathElement.setAttribute("stroke-width", "2");
        pathElement.setAttribute("d", "M20,15 C19,16 21.25,18.75 20,20 C18.75,21.25 16,19 15,20 C14,21 13.5,23 12,23 C10.5,23 10,21 9,20 C8,19 5.25,21.25 4,20 C2.75,18.75 5,16 4,15 C3,14 1,13.5 1,12 C1,10.5 3,10 4,9 C5,8 2.75,5.25 4,4 C5.25,2.75 8,5 9,4 C10,3 10.5,1 12,1 C13.5,1 14,3 15,4 C16,5 18.75,2.75 20,4 C21.25,5.25 19,8 20,9 C21,10 23,10.5 23,12 C23,13.5 21,14 20,15 Z M7,12 L10,15 L17,8");

        // Append path to SVG
        svgElement.appendChild(pathElement);

        svgElement.addEventListener('click', function () {

            if (checkFolderName(addFolder.value)) {
                saveFolderName(addFolder.value, 0);
                displayCustomMenu(name, link, collection, folder, call);
            }
            else {
                alert("un dossier de ce nom existe déjà (ou vous avez nommé votre dossier 'racine', ce qui est interdit pour des raisons techniques)");
            }
            addFolder.value = '';
        });

        addFolderParent.appendChild(addFolder);
        addFolderParent.appendChild(svgElement);
        modalContent.appendChild(addFolderParent);

        var root = document.createElement('div');
        root.className = 'root';
        root.textContent = "racine";
        root.className = 'custom-menu-item';
        root.style.color = '#71c2fb';  // Set text color
        root.style.padding = '11px 20px';  // Set padding
        root.style.fontSize = '14px';  // Set font size
        root.style.textAlign = 'center';  // Set text alignment
        root.style.cursor = 'pointer';  // Set cursor style
        root.addEventListener('click', function () {
            modal.remove();
            document.body.style.overflow = 'auto'; // Allow scrolling
        });

        // Add styles on hover
        root.addEventListener('mouseover', function () {
            root.style.background = '#232e39';
        });

        root.addEventListener('mouseout', function () {
            root.style.background = 'initial'; // Revert to the original background color on mouseout
        });

        modalContent.appendChild(root);


        // Create and append items for each folder
        folderNames.forEach(function (folderName) {
            var menuItem = document.createElement('div');
            menuItem.className = 'custom-menu-item';
            menuItem.textContent = folderName;
            menuItem.style.color = '#71c2fb';  // Set text color
            menuItem.style.padding = '11px 20px';  // Set padding
            menuItem.style.fontSize = '14px';  // Set font size
            menuItem.style.textAlign = 'center';  // Set text alignment
            menuItem.style.cursor = 'pointer';  // Set cursor style

            // Add styles on hover
            menuItem.addEventListener('mouseover', function () {
                menuItem.style.background = '#232e39';
            });

            menuItem.addEventListener('mouseout', function () {
                menuItem.style.background = 'initial'; // Revert to the original background color on mouseout
            });

            menuItem.addEventListener('click', function () {
                // Handle the click on the folder item (e.g., add sticker to the selected folder)
                updateStickersPositionOnFolderChangeOrDelete(name);
                modifyStickerLocalstorage(name, link, collection, folderName);
                setStickerNewPosition(name, getFolderLength(folderName));
                modal.remove();
                document.body.style.overflow = 'auto'; // Allow scrolling
                if (call == 1) {
                    setFavoritestickers(stickersContainer);
                }
                else {
                    displayStickers(call);
                }
            });
            modalContent.appendChild(menuItem);
        });

        if (call != 1) {
            // Get the length of the folder
            const folderLength = getFolderLength(folder);

            // Get the sticker's current position
            const currentPosition = getStickerPosition(name);

            // Create a select element
            const selectList = document.createElement('select');
            selectList.id = 'position-select';
            selectList.style.textAlign = 'center'; // Center align the text
            selectList.style.marginBottom = '23px';
            selectList.style.marginTop = '23px';


            // Create a label element
            const label = document.createElement('label');
            label.textContent = 'Modifiez la position du sticker : ';
            label.setAttribute('for', 'position-select');
            label.style.color = 'white'; // Apply color: white;
            label.style.padding = '11px 20px'; // Apply padding: 11px 20px;
            label.style.fontSize = '14px'; // Apply font-size: 14px;
            label.style.textAlign = 'center'; // Apply text-align: center;

            // Populate the select list with options ranging from 1 to folderLength
            for (let i = 1; i <= folderLength; i++) {
                const option = document.createElement('option');
                option.value = i;
                option.text = i;
                selectList.add(option);
            }

            // Set the default selected value based on the sticker's current position
            selectList.value = currentPosition;

            // Add an event listener to handle the click event
            selectList.addEventListener('change', function () {
                // Get the new selected position
                const newPosition = parseInt(selectList.value);

                // Call setNewPosition function with the chosen value
                setStickerNewPosition(name, newPosition);
                removeFolderStructure();
                buildContainer();
                displayStickers(folder);
            });

            modalContent.appendChild(label);
            modalContent.appendChild(selectList);

            //---------------------------------------------------------------------------------

            var deleteSticker = document.createElement('div');
            deleteSticker.className = 'custom-menu-item';
            deleteSticker.textContent = 'Supprimer ce sticker';
            deleteSticker.style.color = 'white';  // Set text color
            deleteSticker.style.padding = '11px 20px';  // Set padding
            deleteSticker.style.fontSize = '14px';  // Set font size
            deleteSticker.style.textAlign = 'center';  // Set text alignment
            deleteSticker.style.cursor = 'pointer';  // Set cursor style
            deleteSticker.style.backgroundColor = '#bd0e0e';


            deleteSticker.addEventListener('mouseover', function () {
                deleteSticker.style.background = '#ef1616';
            });

            deleteSticker.addEventListener('mouseout', function () {
                deleteSticker.style.background = '#bd0e0e'; // Revert to the original background color on mouseout
            });

            deleteSticker.addEventListener('click', function () {
                modal.remove();
                document.body.style.overflow = 'auto'; // Allow scrolling
                deleteStickerOnCloud(name);
                deleteStickerLocalstorage(name);
                removeFolderStructure();
                buildContainer();
                displayStickers(folder);
            });

            modalContent.appendChild(deleteSticker);

            var modaldiv = document.querySelector(".modal");
            var title = modaldiv.querySelector(".title");
            title.textContent = "Sélectionnez le nouveau dossier";
            var customMenuItems = document.querySelectorAll('.custom-menu-item');
            customMenuItems.forEach(function (menuItem) {
                // Check if the text content matches the specified string
                if (menuItem.textContent.trim() == folder) {
                    // Remove the node
                    menuItem.parentNode.removeChild(menuItem);
                }
            });
        }

        // Add styles dynamically
        const styles = `
            body {
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 0;
                display: flex;
                justify-content: center;
                align-items: center;
                height: 100vh;
                background-color: #f4f4f4;
            }

            .modal {
                display: none;
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.5);
                overflow: scroll;
            }

            .modal-content {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
                background-color: #fff;
                padding: 20px;
                border-radius: 8px;
                box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            }

            .close {
                position: absolute;
                top: 1px;
                right: 1px;
                font-size: 20px;
                cursor: pointer;
            }
            `;

        const styleElement = document.createElement('style');
        styleElement.textContent = styles;
        document.head.appendChild(styleElement);

        modal.style.display = 'block';
        document.body.style.overflow = 'hidden'; // Prevent scrolling

        closeBtn.addEventListener('click', function () {
            modal.remove();
            document.body.style.overflow = 'auto'; // Allow scrolling
        });

        window.addEventListener('click', function (event) {
            if (event.target === modal) {
                modal.remove();
                document.body.style.overflow = 'auto'; // Allow scrolling
            }
        });

        function isElementOverflowing(element) {
            // Get the dimensions and position of the element
            var elementRect = element.getBoundingClientRect();

            // Get the dimensions of the viewport
            var viewportWidth = window.innerWidth || document.documentElement.clientWidth;
            var viewportHeight = window.innerHeight || document.documentElement.clientHeight;

            // Check if any part of the element is outside the viewport
            var isOverflowing =
                elementRect.left < 0 ||
                elementRect.right > viewportWidth ||
                elementRect.top < 0 ||
                elementRect.bottom > viewportHeight;

            return isOverflowing;
        }

        var myElement = document.querySelector(".modal-content"); // replace 'yourElementId' with the actual ID of your element
        var elementIsOverflowing = isElementOverflowing(myElement);
        if (elementIsOverflowing) {
            myElement.style.transform = 'translate(-50%, -10%)';
        }
    }

    //ajouter un sticker au localstorage
    function addStickerLocalstorage(name, link, collection, folder, isCollection) {
        var stickers = JSON.parse(localStorage.getItem('stickfolders')) || [];
        const position = getFolderLength(folder) + 1;
        // Create a new object
        var newEntry = {
            name: name,
            link: link,
            collection: collection,
            folder: folder,
            position: position,
            isCollection: isCollection
        };

        // Add the new object to the array
        stickers.push(newEntry);
        localStorage.setItem('stickfolders', JSON.stringify(stickers));
    }

    //modifier un sticker du localstorage en fonction de son som
    function modifyStickerLocalstorage(name, link, collection, folder) {
        var stickers = JSON.parse(localStorage.getItem('stickfolders')) || [];

        var stickerIndex = stickers.findIndex(sticker => sticker.name === name);

        if (stickerIndex !== -1) {
            // Update the properties of the menu at the found index
            stickers[stickerIndex].link = link;
            stickers[stickerIndex].collection = collection;
            stickers[stickerIndex].folder = folder;

            // Save the modified array back to localStorage
            localStorage.setItem('stickfolders', JSON.stringify(stickers));
        }
    }

    //changer le dossier de tous les stickers appartenant à un certain dossier
    function modifyStickersFolder(targetFolder, newFieldValue) {
        let stickfolders = localStorage.getItem('stickfolders');
        let newFolderLength = 0;
        // Check if the 'stickfolders' item exists in local storage
        if (stickfolders) {
            // Parse the JSON string to an object
            stickfolders = JSON.parse(stickfolders);

            if (newFieldValue == 'racine') {
                newFolderLength = getFolderLength(newFieldValue);
                newFolderLength++;
            }

            // Loop through each item in stickfolders
            for (let i = 0; i < stickfolders.length; i++) {
                // Check if the folder field matches the target folder
                if (stickfolders[i].folder === targetFolder) {
                    // Update the folder field with the new value
                    stickfolders[i].folder = newFieldValue;
                    if (newFieldValue == 'racine') {
                        stickfolders[i].position = newFolderLength;
                    }
                    newFolderLength++;
                }
            }

            // Save the updated stickfolders object back to local storage
            localStorage.setItem('stickfolders', JSON.stringify(stickfolders));
        }
    }

    //supprimer un sticker du localstorage
    function deleteStickerLocalstorage(name) {
        updateStickersPositionOnFolderChangeOrDelete(name);
        var stickers = JSON.parse(localStorage.getItem('stickfolders')) || [];

        var stickerIndex = stickers.findIndex(sticker => sticker.name === name);

        if (stickerIndex !== -1) {

            stickers.splice(stickerIndex, 1);

            // Save the modified array back to localStorage
            localStorage.setItem('stickfolders', JSON.stringify(stickers));
        }
    }

    function deleteStickerOnCloud(name) {
        const url = 'https://onche.org/stickers/favorite/remove';
        // Get the value of the "stickerToken" item from local storage
        const token = localStorage.getItem('stickertoken');

        // Check if the stickerToken exists
        if (token == null) {
            window.alert("[Stickers+] Pour une raison technique, veuillez vous rendre sur un forum ou un topic avant d'effectuer cette action");
        }

        //const data = new URLSearchParams({ name, token }); // Create URLSearchParams object
        const data = new URLSearchParams({ name, token }); // Create URLSearchParams object


        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: data.toString(), // Convert URLSearchParams to string
        })
    }

    //détecte quand un sticker est supprimé via les moyens conventionnels du site
    function detectStickerRemove() {
        var open = XMLHttpRequest.prototype.open;
        var send = XMLHttpRequest.prototype.send;

        XMLHttpRequest.prototype.open = function () {
            // Call the original open function
            open.apply(this, arguments);

            // Store the request URL
            var url = arguments[1];

            // Intercept the send method to capture sent data
            this.send = function () {
                // Check if the request URL matches the desired URL
                if (url === 'https://onche.org/stickers/favorite/remove') {
                    var queryString = arguments[0];
                    var sentData = {};
                    queryString.split('&').forEach(function (keyValue) {
                        var parts = keyValue.split('=');
                        sentData[decodeURIComponent(parts[0])] = decodeURIComponent(parts[1]);
                    });

                    // Get the value of the 'name' field
                    var name = sentData['name'];
                    deleteStickerLocalstorage(name);
                }
                // Call the original send function
                return send.apply(this, arguments);
            };
        };
    }

    // Call the function to start detecting sticker remove requests
    detectStickerRemove();



    /*
        -------------------------------------------------------------------------------------------------------------------
        THIS PART MANAGES THE DISPLAY OF STICKERS IN FAVORITE STICKERS FIELD
        -------------------------------------------------------------------------------------------------------------------
    */

    //allow to setup the favoritestickers field to display folders and stickers
    function setFavoritestickers(target) {
        clearFavoritestickersField(target);
        showFolders(target);
        showStickers('racine', target);
    }

    //clear favoritestickers field
    function clearFavoritestickersField(favoriteStickers) {
        // allow to remove all children from field containing favorites stickers
        if (favoriteStickers) {
            // Remove all child elements of the parent div
            while (favoriteStickers.firstChild) {
                favoriteStickers.removeChild(favoriteStickers.firstChild);
            }
            favoriteStickers.style.display = "flex";
        }
    }

    //show folders in favoritestickers field
    function showFolders(container) {
        // Get the string from localStorage
        var folderNames = localStorage.getItem('folders');

        // Check if there is any data in localStorage
        if (folderNames && folderNames.length > 0) {
            var folderNamesArray = JSON.parse(folderNames);

            // Loop through the folder names
            folderNamesArray.forEach(function (folderName) {
                var folderDiv = document.createElement('div');
                folderDiv.className = 'folder';
                folderDiv.style.display = 'block';
                folderDiv.style.cursor = 'pointer';
                folderDiv.style.fontSize = '1rem';

                // Create an img element
                var imgElement = document.createElement('img');
                imgElement.className = 'image';
                imgElement.src = 'https://codeberg.org/Annapurna/StickersPlus/raw/branch/main/folder.png'
                imgElement.style.height = '3rem';

                var folderDivText = document.createElement('div');
                folderDivText.textContent = folderName;
                folderDiv.style.textAlign = 'center';

                // Append the img element to the folder div
                folderDiv.appendChild(imgElement);

                // Append the img element to the folder div
                folderDiv.appendChild(folderDivText);

                // Append the folder div to the container
                container.appendChild(folderDiv);

                folderDiv.addEventListener('click', function () {
                    showFolderContent(folderName, container);
                });

            });
        }
    }

    //show stickers to corresponding foldername in favoritestickers field
    function showStickers(foldername, container) {
        var localStorageData = localStorage.getItem('stickfolders');

        // Check if there is any data in localStorage
        if (localStorageData) {
            // Parse the JSON string into an array of objects
            var folders = JSON.parse(localStorageData);

            // Filter the array to get objects where the 'folder' field is 'racine'
            var racineFolders = folders.filter(function (item) {
                return item.folder === foldername;
            });

            // Sort correspondingStickers by the 'position' field
            racineFolders.sort(function (a, b) {
                return a.position - b.position;
            });

            // Loop through the filtered items and create HTML elements
            racineFolders.forEach(function (item) {
                // Create a new div element
                var stickerDiv = document.createElement('div');

                // Add classes and data attributes
                stickerDiv.className = 'sticker favorite';
                stickerDiv.setAttribute('data-name', item.name);
                stickerDiv.setAttribute('data-collection', item.collection);

                // Create an img element
                var imgElement = document.createElement('img');

                // Add class and set the src attribute
                imgElement.className = 'image';
                imgElement.src = item.link; // Assuming 'link' contains the image source

                // Append the img element to the sticker div
                stickerDiv.appendChild(imgElement);

                // Append the sticker div to the container
                container.appendChild(stickerDiv);

                if (window.location.href.startsWith("https://onche.org/chat/")) {
                    stickerDiv.style.cursor = "pointer";

                    stickerDiv.addEventListener('click', function () {
                        // Get the textarea element with class 'Form__input'
                        var textarea = document.querySelector('.Form__input');

                        textarea.value += ' :' + item.name + ': ';
                    });
                }
            });
        }
    }

    //show stickers to corresponding foldername in favoritestickers field and a way to go back to root
    function showFolderContent(name, container) {
        clearFavoritestickersField(container);
        var folderDiv = document.createElement('div');
        folderDiv.className = 'folder';
        folderDiv.style.display = 'block';
        folderDiv.style.cursor = 'pointer';

        // Create an img element
        var imgElement = document.createElement('img');
        imgElement.className = 'image';
        imgElement.src = 'https://codeberg.org/Annapurna/StickersPlus/raw/branch/main/folder.png'
        imgElement.style.height = '3rem';

        var folderDivText = document.createElement('div');
        folderDivText.textContent = '..';
        folderDiv.style.textAlign = 'center';

        // Append the img element to the folder div
        folderDiv.appendChild(imgElement);

        // Append the img element to the folder div
        folderDiv.appendChild(folderDivText);

        // Append the folder div to the container
        container.appendChild(folderDiv);

        folderDiv.addEventListener('click', function () {
            setFavoritestickers(container);
        });
        showStickers(name, container);
    }

    // Select all elements with class 'message'
    // Store processed 'message-form' elements
    const processedElements = new Set();

    if (window.location.href.startsWith("https://onche.org/topic/")) {
        // Check for new messages every 5 seconds (adjust interval as needed)
        setInterval(checkForNewMessages, 1000);
    }

    // Function to check for the appearance of 'message-form' elements
    function checkForNewMessages() {
        const messages = document.querySelectorAll('.message');

        // Loop through all '.message' elements
        messages.forEach(message => {
            // Check if 'message' contains any child element with class 'message-form'
            const messageForm = message.querySelector('.message-form');
            if (messageForm && !processedElements.has(messageForm)) {
                // 'message-form' is found within this 'message' element
                const container = messageForm.querySelector('.favoriteStickers');


                // Check if the value is 'true'
                if (stickersNoScrollValue === 'true') {
                    container.style.maxHeight = 'none';
                }

                const folders = container.querySelectorAll('.folder');
                folders.forEach(folder => {
                    const name = folder.querySelector('div').textContent;
                    folder.addEventListener('click', function () {
                        showFolderContent(name, container);
                    });
                });
                // Add the element to the set of processed elements
                processedElements.add(messageForm);
            }
            else if (!messageForm && processedElements.has(messageForm)) {
                // Remove the element from the set of processed elements
                processedElements.delete(messageForm);
            }
        });
    }

    function checkFavoritesPanelOpening() {
        var open = XMLHttpRequest.prototype.open;
        XMLHttpRequest.prototype.open = function () {
            // Call the original open function
            open.apply(this, arguments);
            // Intercept the response
            this.addEventListener('readystatechange', async function () {
                if (this.readyState === 4 && this.status === 200 && this.responseURL === 'https://onche.org/stickers/search') {
                    // Handle the response data
                    var responseData = JSON.parse(this.responseText);

                    // Extract the arrays from the response
                    var popularsArray = responseData.populars;
                    var topArray = responseData.top;
                    var recentsArray = responseData.recents;

                    // Now you have each array stored separately
                    //console.log(popularsArray, topArray, recentArray);

                    setTimeout(function () {
                        const firstElement = document.getElementById('tooltip');
                        const firstChild = firstElement.querySelector('.content');
                        const inputSearch = firstChild.querySelector('.input');
                        const imagesContainer = firstChild.querySelectorAll('.images');

                        // Check if the listElement exists
                        if (popularsArray && popularsArray.length > 0) {
                            setFavoritestickers(stickersContainer);
                            // Iterate through each element in imagesContainer
                            // Select all elements with the class 'images'


                            // Create an array to store the cloned title nodes
                            const clonedTitlesArray = [];
                            const clonedImagesNode = [];

                            // Iterate through each element in imagesContainer
                            imagesContainer.forEach((element, index) => {
                                // Skip the last element
                                if (index === imagesContainer.length - 1) {
                                    // Optionally, you can still remove the last element if needed
                                    element.remove();
                                    return;  // Skip storing the last cloned elements
                                }

                                // Find the child node with the class 'title'
                                const titleNode = element.querySelector('.title');
                                if (titleNode) {
                                    // Clone the title node
                                    const clonedTitle = titleNode.cloneNode(true);

                                    // Store the cloned title in the array (excluding the last one)
                                    clonedTitlesArray.push(clonedTitle);

                                    // Remove the title node from the original element
                                    titleNode.remove();
                                }

                                // Store the cloned images node in the array (excluding the last one)
                                clonedImagesNode.push(element);  // Store the original element

                                // Remove the original element from the DOM
                                element.remove();
                            });

                            setTitles(clonedTitlesArray, clonedImagesNode, firstChild);

                            const firstClonedImageNode = clonedImagesNode[0]; // Get the first cloned node
                            firstChild.appendChild(firstClonedImageNode);
                        }
                    }, 500);
                }
            });
        };
    }

    function setTitles(clonedTitlesArray, clonedImagesNode, firstChild) {
        const firstClonedImageNode = clonedImagesNode[0]; // Get the first cloned node
        const secondClonedImageNode = clonedImagesNode[1]; // Get the first cloned node
        const thirdClonedImageNode = clonedImagesNode[2]; // Get the first cloned node

        // Create a div with the class name 'titleDiv'
        const titleDiv = document.createElement('div');
        titleDiv.className = 'titleDiv';

        // Set the display of the div to flex
        titleDiv.style.display = 'flex';

        // Assuming clonedTitlesArray is already populated
        clonedTitlesArray.forEach((clonedNode, index) => {
            // Apply the specified CSS styles to each cloned node
            clonedNode.style.background = '#1c2229';
            clonedNode.style.color = '#fff';
            clonedNode.style.borderColor = '#1c2229';
            clonedNode.style.borderRadius = '999px';
            clonedNode.style.margin = '4px';
            clonedNode.style.padding = '9px 6px';
            clonedNode.style.cursor = 'pointer';



            // If it's the first cloned node, you can still apply distinct styling (e.g., make it bold)
            if (index === 0) {
                clonedNode.style.fontWeight = 'bold'; // Making the first cloned node bold
                clonedNode.style.background = '#71c2fb';
            }

            // Append each cloned node to the 'titleDiv'
            titleDiv.appendChild(clonedNode);

            // Add an event listener to each cloned node
            clonedNode.addEventListener('click', () => {
                clearField(firstChild);
                // Reset styles for all cloned nodes
                clonedTitlesArray.forEach((node) => {
                    node.style.fontWeight = 'normal'; // Reset font weight
                    node.style.background = '#1c2229'; // Reset background
                });
                switch (index) {
                    case 0:
                        clonedNode.style.fontWeight = 'bold'; // Making the first cloned node bold
                        clonedNode.style.background = '#71c2fb';
                        firstChild.appendChild(firstClonedImageNode);
                        break;
                    case 1:
                        clonedNode.style.fontWeight = 'bold'; // Making the first cloned node bold
                        clonedNode.style.background = '#71c2fb';
                        firstChild.appendChild(secondClonedImageNode);
                        break;
                    case 2:
                        clonedNode.style.fontWeight = 'bold'; // Making the first cloned node bold
                        clonedNode.style.background = '#71c2fb';
                        firstChild.appendChild(thirdClonedImageNode);
                        break;

                    default:
                        break;
                }
            });
        });

        firstChild.appendChild(titleDiv);
    }

    function clearField(content) {
        const imagesChild = content.querySelector('.images');
        imagesChild.remove();
    }

    checkFavoritesPanelOpening();


    /*
        -------------------------------------------------------------------------------------------------------------------
        THIS PART MANAGES THE CONFIGURATION AND MANAGEMENT OF FOLDERS AND STICKERS
        -------------------------------------------------------------------------------------------------------------------
    */

    function getFolderLength(folderName) {
        // Retrieve items from local storage
        const stickfoldersArr = JSON.parse(localStorage.getItem('stickfolders'));

        let folderCount = 0;

        if (stickfoldersArr) {
            // Count the number of objects with 'folder' field matching the given value
            stickfoldersArr.forEach(item => {
                if (item.folder === folderName) {
                    folderCount++;
                }
            });
            return folderCount;
        }
    }

    function getStickerPosition(sticker) {
        // Retrieve items from local storage
        const stickfoldersArr = JSON.parse(localStorage.getItem('stickfolders'));

        let positionValue = null;

        if (stickfoldersArr) {
            // Find the object with the given 'name' field
            const matchingObject = stickfoldersArr.find(item => item.name === sticker);

            if (matchingObject) {
                // Retrieve the 'position' field value
                positionValue = matchingObject.position;
                return positionValue;
            }
        }
    }

    function setStickerNewPosition(sticker, newPosition) {
        // Retrieve items from local storage
        let stickfoldersArr = JSON.parse(localStorage.getItem('stickfolders'));

        if (stickfoldersArr) {
            // Find the sticker object by name
            const stickerObjectIndex = stickfoldersArr.findIndex(item => item.name === sticker);

            if (stickerObjectIndex !== -1) {
                const stickerObject = stickfoldersArr[stickerObjectIndex];
                const originalPosition = stickerObject.position;
                const stickerFolder = stickerObject.folder;

                // No need to proceed if newPosition is equal to originalPosition
                if (newPosition === originalPosition) {
                    return;
                }

                // Update sticker's position
                stickfoldersArr[stickerObjectIndex].position = newPosition;

                // Increment or decrement other objects' positions based on their position relative to sticker's original and new positions
                stickfoldersArr.forEach(item => {
                    if (item.folder === stickerFolder && item.name !== sticker) {
                        if ((originalPosition < newPosition && item.position > originalPosition && item.position <= newPosition) ||
                            (originalPosition > newPosition && item.position >= newPosition && item.position < originalPosition)) {
                            item.position += (originalPosition < newPosition) ? -1 : 1;
                        }
                    }
                });

                // Update the 'stickfolders' item in local storage
                localStorage.setItem('stickfolders', JSON.stringify(stickfoldersArr));
            }
        }
    }


    function updateStickersPositionOnFolderChangeOrDelete(sticker) {
        // Retrieve items from local storage
        let stickfoldersArr = JSON.parse(localStorage.getItem('stickfolders'));

        if (stickfoldersArr) {
            // Find the object with the given 'name' field
            const matchingObject = stickfoldersArr.find(item => item.name === sticker);

            if (matchingObject) {
                const givenFolder = matchingObject.folder;
                const givenPosition = matchingObject.position;

                // Decrement 'position' field for other objects with the same 'folder' and higher 'position'
                stickfoldersArr.forEach(item => {
                    if (item.folder === givenFolder && item.position > givenPosition) {
                        item.position--;
                    }
                });

                // Update the 'stickfolders' item in local storage
                localStorage.setItem('stickfolders', JSON.stringify(stickfoldersArr));

            }
        }
    }


    // Ajoute des dossiers et les affiche
    function buildContainer() {
        // Initialize folders in localStorage if it doesn't exist
        if (!localStorage.getItem('folders')) {
            localStorage.setItem('folders', JSON.stringify([]));
        }

        // Select the container with id "left"
        var leftContainer = document.getElementById('left');

        // Check if the container exists
        if (leftContainer) {
            // Append the folder HTML to the "left" container
            appendFolderStructure(leftContainer);
            var myFolders = document.querySelectorAll('.category');
            myFolders.forEach(function (folder) {
                folder.addEventListener('click', function () {
                    var categoryLabelElement = folder.querySelector('.category-label');
                    var categoryLabelText = categoryLabelElement.textContent;
                    displayStickers(categoryLabelText);
                });
            });
        }

        // Select the "addfolder" div
        var addFolderButton = document.getElementById('addfolder');

        // Check if the "addfolder" div exists
        if (addFolderButton) {
            // Add event listener to the "addfolder" div
            addFolderButton.addEventListener('click', function () {
                // Prompt the user for the folder name
                var folderName = prompt('Entrez le nom du dossier (les doublons ainsi que le nom "racine" sont interdits');

                // Check if the user entered a folder name
                if (folderName) {
                    // Save the folder name to localStorage
                    if (!checkFolderName(folderName)) {
                        alert("un dossier de ce nom existe déjà (ou vous avez nommé votre dossier 'racine', ce qui est interdit pour des raisons techniques)");
                    }
                    else {
                        saveFolderName(folderName, 1);
                    }
                }
            });
        }
    }

    //permet d'afficher le container qui contiendra les dossiers dans /stickers
    function appendFolderStructure(container) {
        // Create a new div element with the provided HTML structure
        var folderDiv = document.createElement('div');
        folderDiv.innerHTML = `
            <div class="bloc" id="folders">
                <div class="title">
                    <span class="titleclass">Mes dossiers</span>
                    <div class="right">
                        <div class="mdi mdi-plus-circle clickable" id="addfolder"></div>
                    </div>
                </div>
                <div class="content categories" id="foldersList">
                    <a class="category">
                        <div class="category-images">
                            <div class="mdi"><img src="https://codeberg.org/Annapurna/StickersPlus/raw/branch/main/folder.png" width="42" height="42"/></div>
                        </div>
                        <div class="category-label">racine</div>
                    </a>
                    ${generateFolderLinks()} <!-- Dynamic generation of folder links -->
                </div>
            </div>
        `;

        // Insert the new folder div at the first position in the "left" container
        container.insertBefore(folderDiv, container.firstChild);
    }

    function removeFolderStructure() {
        var structure = document.getElementById("folders");
        structure.remove();
    }

    //permet d'afficher les dossiers dans /stickers
    function generateFolderLinks() {
        // Retrieve folder names from localStorage
        var folderNames = JSON.parse(localStorage.getItem('folders')) || [];

        // Generate HTML for each folder link
        var folderLinksHTML = folderNames.map(function (folderName) {
            return `
                    <a class="category">
                        <div class="category-images">
                            <div class="mdi"><img src="https://codeberg.org/Annapurna/StickersPlus/raw/branch/main/folder.png" width="42" height="42"/></div>
                        </div>
                        <div class="category-label">${folderName}</div>
                    </a>
                `;
        }).join(''); // Join the array of HTML strings into a single string

        return folderLinksHTML;
    }

    //permet d'enregistrer un dossier nouvellement créé dans le localstorage puis d'appeler la fonction pour l'afficher
    function saveFolderName(folderName, call) {
        var modalTest = document.querySelector(".modal"); // Does not match anything
        if (modalTest) {
            modalTest.remove();
        }
        // Get existing folder names from localStorage or initialize an empty array
        var existingFolders = JSON.parse(localStorage.getItem('folders')) || [];

        // Add the new folder name to the array
        existingFolders.push(folderName);

        // Save the updated array back to localStorage
        localStorage.setItem('folders', JSON.stringify(existingFolders));

        if (call == 1) {
            // Update the UI to display the new folder link
            updateFolderLinks();
        }
    }

    //permet d'afficher un dossier nouvellement ajouté dans /stickers
    function updateFolderLinks() {
        // Check if a div with classname 'folderTitle' exists
        var folderTitleElement = document.querySelector('.folderTitleContent');

        if (folderTitleElement) {
            displayStickers(folderTitleElement.textContent)
        } else {
            removeFolderStructure();
            buildContainer();
        }
    }

    function modifyFolderName(targetValue, newValue) {
        let folders = localStorage.getItem('folders');

        // Check if the 'folders' item exists in local storage
        if (folders) {
            // Parse the JSON string to an array
            folders = JSON.parse(folders);

            // Find the index of the target value in the array
            const index = folders.indexOf(targetValue);

            // Check if the target value was found
            if (index !== -1) {
                // Update the value at the found index with the new value
                folders[index] = newValue;

                // Save the updated folders array back to local storage
                localStorage.setItem('folders', JSON.stringify(folders));
            }
        }
    }

    function deleteFolder(targetValue) {
        let folders = localStorage.getItem('folders');

        // Check if the 'folders' item exists in local storage
        if (folders) {
            // Parse the JSON string to an array
            folders = JSON.parse(folders);

            // Find the index of the target value in the array
            const index = folders.indexOf(targetValue);

            // Check if the target value was found
            if (index !== -1) {
                // Use splice to remove the element at the found index
                folders.splice(index, 1);

                // Save the updated folders array back to local storage
                localStorage.setItem('folders', JSON.stringify(folders));
            }
        }
    }

    function checkFolderName(targetValue) {
        if (targetValue == 'racine' || targetValue == 'Racine') {
            return false;
        }

        let folders = localStorage.getItem('folders');

        // Check if the 'folders' item exists in local storage
        if (folders) {
            // Parse the JSON string to an array
            folders = JSON.parse(folders);

            // Check if the target value exists in the array
            const exists = folders.includes(targetValue);

            // Return false if the value exists, otherwise return true
            return !exists;
        }

        // Return true if the 'folders' item does not exist
        return true;
    }


    function displayStickers(folderName) {

        var folderNameTracker = folderName;
        var folderContainer = document.getElementById('folders');
        var title = folderContainer.querySelector('.title');
        var titleDiv = title.querySelector('.titleclass');

        titleDiv.innerHTML = '<div class="folderTitle" style="margin-right:15px;"> Dossier : <em class="folderTitleContent">' + folderNameTracker + '</em></div>';
        titleDiv.style.marginRight = "10px";
        var content = document.getElementById('foldersList');
        while (content.firstChild) {
            content.removeChild(content.firstChild);
        }

        if (folderName != 'racine') {

            var editImg = document.createElement('img');
            editImg.src = 'https://codeberg.org/Annapurna/StickersPlus/raw/branch/main/edit.png'; // Replace with the actual path to your image
            editImg.alt = 'modifier'; // Replace with appropriate alt text
            editImg.style.width = "20px";
            editImg.style.cursor = "pointer";
            editImg.style.height = "20px";
            editImg.style.marginRight = "5px";

            titleDiv.appendChild(editImg);

            editImg.addEventListener('mouseover', function () {
                editImg.style.height = "30px";
                editImg.style.width = "30px";
            });

            editImg.addEventListener('mouseout', function () {
                editImg.style.height = "20px";
                editImg.style.width = "20px";
            });


            var deleteImg = document.createElement('img');
            deleteImg.src = 'https://codeberg.org/Annapurna/StickersPlus/raw/branch/main/delete.png'; // Replace with the actual path to your image
            deleteImg.alt = 'supprimer'; // Replace with appropriate alt text
            deleteImg.style.width = "20px";
            deleteImg.style.cursor = "pointer";
            deleteImg.style.height = "20px";
            titleDiv.appendChild(deleteImg);

            deleteImg.addEventListener('mouseover', function () {
                deleteImg.style.height = "30px";
                deleteImg.style.width = "30px";
            });

            deleteImg.addEventListener('mouseout', function () {
                deleteImg.style.height = "20px";
                deleteImg.style.width = "20px";
            });



            var validateImg = document.createElement('img');
            validateImg.src = 'https://codeberg.org/Annapurna/StickersPlus/raw/branch/main/validate.png'; // Replace with the actual path to your image
            validateImg.alt = 'valider'; // Replace with appropriate alt text
            validateImg.style.width = "20px";
            validateImg.style.height = "20px";
            validateImg.style.cursor = "pointer";
            validateImg.style.display = "none";
            validateImg.style.marginRight = "10px";

            validateImg.addEventListener('mouseover', function () {
                validateImg.style.height = "30px";
                validateImg.style.width = "30px";
            });

            validateImg.addEventListener('mouseout', function () {
                validateImg.style.height = "20px";
                validateImg.style.width = "20px";
            });

            titleDiv.appendChild(validateImg);



            var cancel = document.createElement('img');
            cancel.src = 'https://codeberg.org/Annapurna/StickersPlus/raw/branch/main/cancel.png'; // Replace with the actual path to your image
            cancel.alt = 'valider'; // Replace with appropriate alt text
            cancel.style.width = "20px";
            cancel.style.height = "20px";
            cancel.style.cursor = "pointer";
            cancel.style.display = "none";

            cancel.addEventListener('mouseover', function () {
                cancel.style.height = "30px";
                cancel.style.width = "30px";
            });

            cancel.addEventListener('mouseout', function () {
                cancel.style.height = "20px";
                cancel.style.width = "20px";
            });

            titleDiv.appendChild(cancel);


            var inputElement = document.createElement('input');

            editImg.addEventListener('click', function () {
                // Get the reference to the div element
                var folderTitleDiv = document.querySelector('.folderTitleContent');

                // Create an input element

                // Set the input element's type to text
                inputElement.type = 'text';

                // Set the input element's value to the current text content of the div
                inputElement.value = folderTitleDiv.textContent.trim();

                // Replace the div with the input element
                folderTitleDiv.innerHTML = '';
                folderTitleDiv.appendChild(inputElement);

                deleteImg.style.display = "none";
                editImg.style.display = "none";
                validateImg.style.display = "block";
                cancel.style.display = "block";

            });

            deleteImg.addEventListener('click', function () {
                deleteFolder(folderNameTracker);
                modifyStickersFolder(folderNameTracker, 'racine');
                removeFolderStructure();
                buildContainer();
            });

            validateImg.addEventListener('click', function () {
                var folderTitleDiv = document.querySelector('.folderTitleContent');
                if (checkFolderName(inputElement.value)) {
                    folderTitleDiv.innerHTML = inputElement.value;
                    modifyStickersFolder(folderNameTracker, inputElement.value);
                    modifyFolderName(folderNameTracker, inputElement.value);
                    folderNameTracker = inputElement.value;
                }
                else {
                    alert("un dossier de ce nom existe déjà (ou vous avez nommé votre dossier 'racine', ce qui est interdit pour des raisons techniques)");
                }

                editImg.style.display = "block";
                deleteImg.style.display = "block";
                validateImg.style.display = "none";
                cancel.style.display = "none";
            });

            cancel.addEventListener('click', function () {
                var folderTitleDiv = document.querySelector('.folderTitleContent');
                folderTitleDiv.innerHTML = folderNameTracker;
                editImg.style.display = "block";
                deleteImg.style.display = "block";
                validateImg.style.display = "none";
                cancel.style.display = "none";
            });
        }

        // Create a div element
        var returnDiv = document.createElement('div');
        returnDiv.style.float = "left";
        returnDiv.style.cursor = "pointer";


        // Create an image element
        var backImg = document.createElement('img');
        backImg.src = 'https://codeberg.org/Annapurna/StickersPlus/raw/branch/main/back.png'; // Replace with the actual path to your image
        backImg.alt = 'retour'; // Replace with appropriate alt text
        backImg.style.width = "50px";
        backImg.style.height = "50px";


        returnDiv.appendChild(backImg);
        // Append the div to the body or any other container element
        content.appendChild(returnDiv);

        // Add an event listener to the div
        returnDiv.addEventListener('click', function () {
            removeFolderStructure();
            buildContainer();
        });


        var localStorageData = localStorage.getItem('stickfolders');
        var stickers = JSON.parse(localStorageData);

        var correspondingStickers = stickers.filter(function (item) {
            return item.folder === folderNameTracker;
        });

        // Sort correspondingStickers by the 'position' field
        correspondingStickers.sort(function (a, b) {
            return a.position - b.position;
        });

        // Loop through the filtered items and create HTML elements
        correspondingStickers.forEach(function (sticker) {

            // Create a new div element
            var stickerDiv = document.createElement('div');

            // Add classes and data attributes
            stickerDiv.className = 'sticker favorite';
            stickerDiv.setAttribute('data-name', sticker.name);
            stickerDiv.setAttribute('data-collection', sticker.collection);
            stickerDiv.setAttribute('data-folder', sticker.folder);

            // Create an img element
            var imgElement = document.createElement('img');

            // Add class and set the src attribute
            imgElement.className = 'image';
            imgElement.src = sticker.link;
            imgElement.style.cursor = 'pointer';

            // Append the img element to the sticker div
            stickerDiv.appendChild(imgElement);

            // Append the sticker div to the container
            content.appendChild(stickerDiv);

            stickerDiv.addEventListener('click', function () {
                displayCustomMenu(sticker.name, sticker.link, sticker.collection, sticker.folder, folderNameTracker)
            });

            // Add styles on hover
            stickerDiv.addEventListener('mouseover', function () {
                imgElement.src = 'https://codeberg.org/Annapurna/StickersPlus/raw/branch/main/config.png';
            });

            stickerDiv.addEventListener('mouseout', function () {
                imgElement.src = sticker.link;
            });
        });
    }

    if (window.location.href === 'https://onche.org/stickers') {
        buildContainer();
    }


    if (window.location.href.startsWith("https://onche.org/account/profil/appearance")) {
        // Create the elements
        const blocDiv = document.createElement('div');
        blocDiv.classList.add('bloc');

        const titleDiv = document.createElement('div');
        titleDiv.classList.add('title');
        titleDiv.textContent = 'Stickers+';

        const contentDiv = document.createElement('div');
        contentDiv.classList.add('content', 'items');

        const itemDiv = document.createElement('div');
        itemDiv.classList.add('item');

        const span = document.createElement('span');
        span.textContent = "Afficher les stickers sans défilement";

        const rightDiv = document.createElement('div');
        rightDiv.classList.add('right');

        const label = document.createElement('label');
        label.classList.add('switch');
        label.setAttribute('data-appearance-parameter', 'stealth-mode');

        const input = document.createElement('input');
        input.setAttribute('type', 'checkbox');

        // Check if the value is 'true'
        if (stickersNoScrollValue === 'true') {
            // Perform your action here, for example:
            input.checked = true;
        }

        // Add event listener to the input element
        input.addEventListener('change', function () {
            // Check if the input is checked
            if (this.checked) {
                // If checked, set the value of 'stickersNoScroll' to 'true' in local storage
                localStorage.setItem('stickersNoScroll', 'true');
            } else {
                // If not checked, set the value of 'stickersNoScroll' to 'false' in local storage
                localStorage.setItem('stickersNoScroll', 'false');
            }
        });

        const switchContentDiv = document.createElement('div');
        switchContentDiv.classList.add('switch_content');

        const p = document.createElement('p');
        p.classList.add('description');
        p.textContent = "Permet d'afficher les stickers en un bloc simple sans barre de défilement.";


        //------------------------------------------------------------------------------

        const itemDivBis = document.createElement('div');
        itemDivBis.classList.add('item');

        const spanBis = document.createElement('span');
        spanBis.textContent = "Synchronisez les stickers ajoutés ou supprimés depuis d'autres appareils";

        const rightDivBis = document.createElement('div');
        rightDivBis.classList.add('right');

        const labelBis = document.createElement('label');
        labelBis.classList.add('switch');
        labelBis.setAttribute('data-appearance-parameter', 'stealth-mode');

        const inputBis = document.createElement('input');
        inputBis.setAttribute('type', 'checkbox');

        // Check if the value is 'true'
        if (stickersSynchronize === 'true') {
            // Perform your action here, for example:
            inputBis.checked = true;
        }

        // Add event listener to the input element
        inputBis.addEventListener('change', function () {
            // Check if the input is checked
            if (this.checked) {
                // If checked, set the value of 'stickersNoScroll' to 'true' in local storage
                localStorage.setItem('stickersynchronize', 'true');
            } else {
                // If not checked, set the value of 'stickersNoScroll' to 'false' in local storage
                localStorage.setItem('stickersynchronize', 'false');
            }
        });

        const switchContentDivBis = document.createElement('div');
        switchContentDivBis.classList.add('switch_content');

        const pBis = document.createElement('p');
        pBis.classList.add('description');
        pBis.textContent = "Permet d'activer ou de désactiver la synchronisation des stickers avec les autres appareils où le même compte est utilisé. Si un sticker est ajouté depuis un autre appareil, il sera mis à la racine dans cet appareil en dernière position";

        //----------------------------------------------------------------------------------------
        // Append the elements to build the structure
        label.appendChild(input);
        label.appendChild(switchContentDiv);

        labelBis.appendChild(inputBis);
        labelBis.appendChild(switchContentDivBis);

        rightDiv.appendChild(label);

        rightDivBis.appendChild(labelBis);

        itemDiv.appendChild(span);
        itemDiv.appendChild(rightDiv);
        itemDiv.appendChild(p);

        itemDivBis.appendChild(spanBis);
        itemDivBis.appendChild(rightDivBis);
        itemDivBis.appendChild(pBis);

        contentDiv.appendChild(itemDiv);
        contentDiv.appendChild(itemDivBis);

        blocDiv.appendChild(titleDiv);
        blocDiv.appendChild(contentDiv);

        // Find the element with id "left"
        const leftDiv = document.getElementById('left');

        // Get the last child element of the "left" div
        const lastChild = leftDiv.lastElementChild;

        // Insert the blocDiv before the last child element
        leftDiv.insertBefore(blocDiv, lastChild);

    }


    /*
        -------------------------------------------------------------------------------------------------------------------
        THIS PART ALLOWS TO INSERT FAVORITE STICKERS IN PRIVATE MESSAGES
        -------------------------------------------------------------------------------------------------------------------
    */

    function createContainer() {
        var parentBox = document.querySelector('.content');

        var favoriteStickersDiv = document.createElement('div');
        favoriteStickersDiv.className = "favoriteStickers";
        favoriteStickersDiv.style.display = 'flex';
        favoriteStickersDiv.style.border = '1px solid #39495c';
        favoriteStickersDiv.style.background = '#232e39';
        favoriteStickersDiv.style.color = '#fff';
        favoriteStickersDiv.style.justifyContent = 'center';
        favoriteStickersDiv.style.padding = '.5rem';
        favoriteStickersDiv.style.width = '100%';
        favoriteStickersDiv.style.columnGap = '.5rem';
        favoriteStickersDiv.style.flexWrap = 'wrap';

        if (stickersNoScrollValue === 'false') {
            // Perform your action here, for example:
            favoriteStickersDiv.style.maxHeight = '7rem';
            favoriteStickersDiv.style.overflow = 'scroll';
        }


        parentBox.appendChild(favoriteStickersDiv);

        return favoriteStickersDiv;
    }

    if (window.location.href.startsWith("https://onche.org/chat/")) {
        const myContainer = createContainer();
        showFolders(myContainer);
        showStickers('racine', myContainer);
    }


    /*
    -------------------------------------------------------------------------------------------------------------------
        THIS PART ALLOWS TO ADD A WHOLE COLLECTION TO FAVORITES
    -------------------------------------------------------------------------------------------------------------------
    */

    function handleCollection() {
        var imagesDiv = document.querySelector('.images');
        var title = imagesDiv.querySelector('.title');
        var titleTextcontent = title.textContent;
        var titleTextcontentValid = titleTextcontent;

        // Create a button element
        var button = document.createElement('button');
        button.textContent = 'Ajoutez cette collection aux favoris';
        button.style.backgroundColor = 'blue';
        button.style.color = 'white';
        button.style.padding = '10px';
        button.style.cursor = "pointer";

        button.addEventListener('click', function () {
            while (!checkFolderName(titleTextcontentValid)) {
                titleTextcontentValid += '-';
            }
            saveFolderName(titleTextcontentValid, 0);

            var images = imagesDiv.querySelectorAll('.image');

            images.forEach(function (image) {
                var imgElement = image.querySelector('img');

                var srcAttribute = imgElement.getAttribute('src');
                var titleAttribute = imgElement.getAttribute('title');
                var modifiedTitle = titleAttribute.substring(1, titleAttribute.length - 1);

                addStickerLocalstorage(modifiedTitle, srcAttribute, titleTextcontent, titleTextcontentValid, true);

            });
            var grandparent = imagesDiv.parentNode.parentNode;
            grandparent.style.display = 'none';
            button.remove();
            setFavoritestickers(stickersContainer);
        });

        // Append the button at the first position of the 'images' div
        imagesDiv.insertBefore(button, title);
    }


    /*
    -------------------------------------------------------------------------------------------------------------------
        BIGSTICKERS
    -------------------------------------------------------------------------------------------------------------------
    */

    if (window.location.href.startsWith("https://onche.org/topic/")) {
        const size = 64; //tailles possibles : 16, 32, 64 (défaut), 128, 256, 512

        function setContainer(element) {
            const container = document.getElementById('context');
            container.appendChild(element);
        }

        function stickModif(sticker) {
            const img = sticker.querySelector('img');
            const urlGet = img.getAttribute('src');
            const url = urlGet.replace(/\d{3}$/, "512");
            const urlBis = urlGet.replace(/\d{3}$/, '');
            const urlReplace = urlGet.replace(/\d{3}$/, size);
            if (size != 64) {
                img.style.maxWidth = '512px';
                img.style.maxHeight = '512px';
                img.style.minWidth = '8px';
                img.src = urlReplace;
            }
            sticker.addEventListener('click', function (event) {
                // Create a new div element with class "item"
                const divElement = document.createElement('div');
                const divElementBis = document.createElement('div');
                divElement.classList.add('item');
                divElementBis.classList.add('item');


                // Create a new anchor element with href attribute of "muyrl.com" and inner text "Afficher en taille réelle"
                const aElement = document.createElement('a');
                const aElementBis = document.createElement('a');

                aElement.href = url;
                aElementBis.href = urlBis;


                aElement.textContent = "Afficher en grand";
                aElementBis.textContent = "Afficher en taille réelle";


                // Append the anchor element to the div element
                divElement.appendChild(aElement);
                divElementBis.appendChild(aElementBis);


                setTimeout(() => {
                    setContainer(divElement);
                    setContainer(divElementBis);
                }, 1);
            });
        }

        const messages = document.querySelectorAll('.message:not(.answer)');
        messages.forEach(message => {
            const stickers = message.querySelectorAll('.sticker');
            if (stickers) {
                stickers.forEach(sticker => {
                    stickModif(sticker);
                });
            }
        });
    }

})();

//TODO

//lors de l'ajout d'une collection, refresh le favorite sticker field
//vérifier que le dossier existe lorsque qu'on y ajoute le sticker
//pouuvoir ajouter ses propres collections
//changer la couleur des icônes en fonction du thème
//pouvoir définir l'ordre pour les dossiers
//pouvoir configurer les stickers directement sur le forum
